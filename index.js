const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use(express.urlencoded({extended: false}))
const cors = require('cors');
app.use(cors());
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-project-db-account:cloud1010@cca-mirandar1.xoiwp.mongodb.net/cca-project?retryWrites=true&w=majority"; 
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to the MongoDB cluster");
});
app.listen(port, () => 
    console.log(`HTTP Server with Exress.js is listening on port:${port}`))

app.get('/', (req, res) => {
    res.send("Useraccount Microservice for Team 6.");
})

//For Testing Signup and Login Microservice
app.get('/signuptest', (req,res) => {
    res.sendFile(__dirname + '/signup.html');
});

/**
 * Signup Microservice
 */
app.post('/signup',(req,res) => {
    const db = dbClient.db();
    const {username,password,fullName,email} = req.body; //input
    /**
     * Username check
     * -must be 3-15 characters
     */
    if(username.length < 3 || username.length > 15) {
        console.log("ERROR: Username must be 3-15 characters");
        res.send({status: "invalid", message: "Username must be 3-15 characters. Try again"});
    }
    /**
     * Password check
     * -must be 3-15 characters
     */
    if(password.length < 3 || password.length > 15) {
        console.log("ERROR: Password must be 3-15 characters");
        res.send({status: "invalid", message: "Password must be 3-15 characters. Try again"});
    }
    /**
     * Full Name Check
     */
    if(fullName.length == 0 || fullName.length > 50) {
        console.log("ERROR: Full Name must be 1-50 characters");
        res.send({status: "invalid", message: "Full Name must be 1-50 characters. Try again"});
    }    
    /**
     * Email Check
     * -must be 1-30 characters
     * -TODO: must follow format of foo@bar.com
     */
    if(email.length == 0 || email.length > 30) {
        console.log("ERROR: Email must be 1-30 characters");
        res.send({status: "invalid", message: "Email must be 1-30 characters. Try again"});
    }
    console.log('/signup POST req: username: ' + username);
    console.log('/signup POST req: password: ' + password);
    console.log('/signup POST req: fullName: ' + fullName);
    console.log('/signup POST req: email: ' + email);
    db.collection("users").findOne({username:username},(err, user)=>{
        if(user) { //if the user already exists 
            console.log("ERROR: username already exists");
            res.send({status: "invalid", message: "Username already exists. Try a different username."});
        }else{ //if user doesn't exist, add to database
            let newUser = {username:username,password:password,fullName:fullName,email:email};
            db.collection("users").insertOne(newUser,(err,result)=> {
                if(err) {
                    console.log("ERROR: Signup Error");
                    res.send({status: "fail", message: "Signup Error for " + newUser.username + ". Please try again."});
                } else {
                    console.log("Signup Successful for " + newUser.username);
                    res.status(200).json({status: "success", message: "New User Successfully Created! " + newUser.username + ", you can now login to the chat!", newUser:newUser});
                }
            });
        }
    });
});

/**
 * Login Microservice
 */
app.get('/login/:username/:password',(req,res)=>{
    const db = dbClient.db();
    const username = req.params.username;
    const password = req.params.password;
    /**
     * Username check
     * -must be 3-15 characters
     */
    if(!username || username.length < 3 || username.length > 15) {
        console.log("ERROR: Username must be 3-15 characters");
        res.send({status: "invalid", message: "Username must be 3-15 characters. Try again"});
    }
    /**
     * Password check
     * -must be 3-15 characters
     */
    if(!password || password.length < 3 || password.length > 15) {
        console.log("ERROR: Password must be 3-15 characters");
        res.send({status: "invalid", message: "Password must be 3-15 characters. Try again"});
    }
    db.collection("users").findOne({username:username, password:password},(err,user) => {
        if(err || !user) {
            //error or user not found
            console.log(`ERROR: User (${username}:${password}) not found`);
            res.send({status: "notfound", message: `ERROR: Username (${username}) and password (${password}) combination not found in database. Please try again or create an account`});
        } 
        if(user && user.username === username) {
            //valid user found and password matches
            console.log(`User (${username}:${password}) found!`);
            res.status(200).json({status: "found", message: `User (${username}) found!`, user:user});
        }
    });
});